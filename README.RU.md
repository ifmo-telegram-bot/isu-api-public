# [ISU API Public [1.0.2 | 21.02.2017]](https://gitlab.com/ifmo-telegram-bot/isu-api-public/blob/d84e9c2cea9ec9f9c19b96ab2bbf80d47125559c/isu_public_1_0_2.yaml) для [Node.js](http://nodejs.org)
Это API предоставляет доступ к расписанию занятий и экзаменов групп и преподавателей, исходя из данных интеллектуальной системы управления университета ИТМО, [ИСУ ИТМО](http://isu.ifmo.ru).
## :small_orange_diamond: Зависимости
* **[Node.js](http://nodejs.org)** - версии 0.3.6 и выше

## :construction: Начало работы
* **Установите** API либо из директории:
```shell
npm install
```
либо с помощью NPM репозитория (:warning: По состоянию на 12.03.2017 isu-api-public отсутствует в репозиториях, и выложен будет позже):
```shell
npm install isu-api-public@1.0.2
```

* **Подключите к вашему проекту и используйте**

Пример получения расписания занятий группы B3205:
```javascript
const ISU_API = new (require('isu-api-public'))('YOUR_ISU_TOKEN');

//Использование с обратным вызовом (callback). JSON-объект будет передан в функцию обратного вызова
ISU_API.get.group.schedule('B3205', function(ResponseData){
    console.log(ResponseData);
});

//Использование с Promises. JSON-объект будет возвращен в "then" блок, а ошибки в "catch" блок.
ISU_API.get.group.schedule('B3205')
    .then((JSON)=>{
        console.log(JSON);
    })
    .catch((error, data)=>{
        console.error(error);
    });
```

## :information_source: Документация к методам ISU Schedule API
:exclamation: Все HTTP запросы относительны адресу *https://isu.ifmo.ru/ords/isurest/v1/api/public*

Класс | Метод | Аналогичный HTTP запрос | Описание
------------ | ------------- | ------------- | -------------
*isu-api-public.ScheduleApi* | **get.week** | **GET** /schedule/week/{key} | Получение номера недели и ее четности.
*isu-api-public.ScheduleApi* | **get.group.schedule** | **GET** /schedule/common/group/{key}/{group} | Получение расписания занятий группы.
*isu-api-public.ScheduleApi* | **get.group.exams** | **GET** /exams/common/group/{key}/{group} | Получение расписания экзаменов группы.
*isu-api-public.ScheduleApi* | **get.teacher.schedule** | **GET** /schedule/common/teacher/{key}/{teacherId} | Получение расписания занятий преподавателя.
*isu-api-public.ScheduleApi* | **get.teacher.exams** | **GET** /exams/common/teacher/{key}/{teacherId} | Получение расписания экзаменов преподавателя.