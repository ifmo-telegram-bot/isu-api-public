# [ISU API Public [1.0.2 | 21.02.2017]](https://gitlab.com/ifmo-telegram-bot/isu-api-public/blob/d84e9c2cea9ec9f9c19b96ab2bbf80d47125559c/isu_public_1_0_2.yaml) for [Node.js](http://nodejs.org)
This API provides access to the schedules of classes and examinations of groups and teachers from the intellectual management system (ISU) of the IFMO University, [ISU IFMO](http://isu.ifmo.ru).

## :small_orange_diamond: Dependencies
* **[Node.js](http://nodejs.org)** - version 0.3.6 and higher

## :construction: Getting started
* **Install** this API from directory:
```shell
npm install
```
or from NPM repositories (:warning: At 12.03.2017 isu-api-public is missing in the repositories, wait for a while):
```shell
npm install isu-api-public@1.0.2
```

* **Prepare and use it in your project**

Example demonstrates preparations and getting B3205 group schedule:
```javascript
const ISU_API = new (require('isu-api-public'))('YOUR_ISU_TOKEN');

//Usage with callback function. JSON-object will be passed to the callback function.
ISU_API.get.group.schedule('B3205', function(ResponseData){
    console.log(ResponseData);
});

//Usage with Promises. JSON-object will be returned to the "then" block, errors to "catch" block.
ISU_API.get.group.schedule('B3205')
    .then((JSON)=>{
        console.log(JSON);
    })
    .catch((error, data)=>{
        console.error(error);
    });
```

## :information_source: Documentation for ISU Schedule API Endpoints
:exclamation: All URIs are relative to *https://isu.ifmo.ru/ords/isurest/v1/api/public*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*isu-api-public.ScheduleApi* | **get.week** | **GET** /schedule/week/{key} | Get the week number and its parity.
*isu-api-public.ScheduleApi* | **get.group.schedule** | **GET** /schedule/common/group/{key}/{group} | Get a group schedule.
*isu-api-public.ScheduleApi* | **get.group.exams** | **GET** /exams/common/group/{key}/{group} | Get a group exams schedule.
*isu-api-public.ScheduleApi* | **get.teacher.schedule** | **GET** /schedule/common/teacher/{key}/{teacherId} | Get a teacher schedule.
*isu-api-public.ScheduleApi* | **get.teacher.exams** | **GET** /exams/common/teacher/{key}/{teacherId} | Get a teacher exams schedule.