/**
 * Created by Irelynx on 12.03.2017.
 * https://gitlab.com/Irelynx
 * https://github.com/Irelynx
 *
 * ISpace:#3
 */

const https = require('https');
module.exports = function(token){
    this.token = token;
    var _this = this;
    this.get = {
        JSONByURL:(token, URI, param, resolver, rejecter)=>{
            var url = 'https://isu.ifmo.ru/ords/isurest/v1/api/public' + URI + token + ((param) ? '/' + param : '');
                https.get(url, (response)=> {
                    var body = '';
                    response.on('data', (chunk)=> {
                        body += chunk;
                    });
                    response.on('end', ()=> {
                        if (body[0] == '<') {
                            console.log(url);
                            return rejecter({e:'HTML', msg:'some data is incorrect, try again', code:-3001}, body);
                        }
                        var JSONResponse = JSON.parse(body);
                        if ((JSONResponse.error) || (JSONResponse.http) || (JSONResponse.code<0)) {
                            return rejecter({e:'JSON', msg:JSONResponse.code + ' code, '+ JSONResponse.http, http:JSONResponse.http, code:-3002}, JSONResponse);
                        }
                        if (typeof resolver === 'function') {
                            resolver(JSONResponse);
                        } else {
                            if (rejecter != undefined) {
                                resolver(JSONResponse);
                            } else {
                                return JSONResponse; // Promise needed...
                            }
                        }
                    });
                }).on('error', (e)=> {
                    console.error(e);
                    if (typeof rejecter == 'function') {
                        rejecter({e:e, code:-3000});
                    } else {
                        return undefined;
                    }
                });
        },
        week:function(callback){
           // console.log(_this.get.JSONByURL(_this.token, '/schedule/week/', '', callback));
            if (typeof callback == 'function') {
                return _this.get.JSONByURL(_this.token, '/schedule/week/', '', callback);
            }
            else {
                return new Promise((resolver, rejecter)=> {
                    _this.get.JSONByURL(_this.token, '/schedule/week/', '', resolver, rejecter);
                });
            }
        },
        group: {
            schedule:(group, callback)=>{
                group = group || '';
                if (typeof callback == 'function') {
                    return _this.get.JSONByURL(_this.token, '/schedule/common/group/', group, callback);
                }
                else {
                    return new Promise((resolver, rejecter)=> {
                        _this.get.JSONByURL(_this.token, '/schedule/common/group/', group, resolver, rejecter);
                    });
                }
            },
            exams:function(group, callback){
                group = group || '';
                if (typeof callback == 'function') {
                    return _this.get.JSONByURL(_this.token, '/exams/common/group/', group, callback);
                }
                else {
                    return new Promise((resolver, rejecter)=> {
                        _this.get.JSONByURL(_this.token, '/exams/common/group/', group, resolver, rejecter);
                    });
                }
            }
        },
        teacher:{
            schedule:function(teacherID, callback){
                teacherID = teacherID || '';
                if (typeof callback == 'function') {
                    return _this.get.JSONByURL(_this.token, '/schedule/common/teacher/', teacherID, callback);
                }
                else {
                    return new Promise((resolver, rejecter)=> {
                        _this.get.JSONByURL(_this.token, '/schedule/common/teacher/', teacherID, resolver, rejecter);
                    });
                }
            },
            exams:function(teacherID, callback){
                teacherID = teacherID || '';
                if (typeof callback == 'function') {
                    return _this.get.JSONByURL(_this.token, '/exams/common/teacher/', teacherID, callback);
                }
                else {
                    return new Promise((resolver, rejecter)=> {
                        _this.get.JSONByURL(_this.token, '/exams/common/teacher/', teacherID, resolver, rejecter);
                    });
                }
            }
        }
    };
    return this;
};